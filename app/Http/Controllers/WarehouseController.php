<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;

class WarehouseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $shop = Auth::user();
        $product_id = $request->product_id;
        $variant_id = $request->variant_id;

        if(empty($product_id)){
            return false;
        }

        $api_version = config('api_version');

        //GET https://{shop}.myshopify.com/admin/api/2021-04/products/{product_id}/variants/{variant_id}.json
        $product_variant = $shop->api()->rest('GET', '/admin/api/'. $api_version . '/products/' . $product_id . '.json');

        $product_variant = $product_variant['body'];

        echo '<pre>';
        print_r($product_variant);

        //GET https://{shop}.myshopify.com/admin/api/2021-04/inventory_levels.json?inventory_item_ids={inventory_item_id}
        $result = $shop->api()->rest('GET', '/admin/api/2020-10/inventory_levels.json?inventory_item_ids=34105178390609');
        // $request = $shop->api()->graph('{ shop { name } }');

        print_r($result);
        die();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
